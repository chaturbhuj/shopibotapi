'use strict';

/**
 * Module dependencies.
 */
const { ValidationError } = require('express-validation');

/**
 * Expose
 */

module.exports = function (app) {
  // Require Routes
  require('../app/routes/bot.route')(app);
  require('../app/routes/stripe.route')(app);
  require('../app/routes/merchant.route')(app);

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json(err);
    } else if (
      err.message &&
      (~err.message.indexOf('not found') ||
        ~err.message.indexOf('Cast to ObjectId failed'))
    ) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).send(err.stack);
  });

  // assume 404 since no middleware responded
  app.use(function (req, res) {
    res.status(404).send('No such url present');
  });
};
