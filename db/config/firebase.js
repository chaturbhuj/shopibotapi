const admin = require('firebase-admin');

const serviceAccount = require('./firebaseServiceAccount.json');

const firebaseApp = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const firebaseAuth = firebaseApp.auth();

module.exports = firebaseAuth;
