/**
 * Expose
 */

module.exports = {
  db: process.env.MONGODB_URL || 'mongodb://localhost:27017/shopibotDB',
  stripeApiKey: process.env.STRIPE_API_KEY || 'sk_test_58SzxfvW1AXXFjMTsJcmq4VN',
  successUrl: process.env.SUCCESS_URL || '',
  cancelUrl: process.env.CANCEL_URL || '',
  encryptionSalt: process.env.ENCRYPTION_SALT || '',
};
