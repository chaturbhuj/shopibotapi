const { SIGNUP_STATUSES, WA_STATUSES } = require('../app/utils/constants');

exports.stringConfig = {
  type: String,
  required: true,
};

exports.objectConfig = {
  type: Object,
  required: true,
};

exports.booleanConfig = {
  type: Boolean,
  required: true,
};

exports.signupConfig = {
  type: String,
  required: true,
  enum: SIGNUP_STATUSES,
};

exports.numberConfig = {
  type: Number,
  required: true,
  default: 0,
};

exports.waStatusConfig = {
  type: String,
  required: true,
  enum: WA_STATUSES,
  default: WA_STATUSES[0],
};

exports.botIdConfig = {
  type: String,
  required: false,
};
