'use strict';

/**
 * Module dependencies
 */

require('dotenv').config();

const fs = require('fs');
const join = require('path').join;
const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const schedule = require('node-schedule');

const models = join(__dirname, 'app/models');
const { checkWabaStatus } = require('./app/utils/WABACron');
const port = process.env.PORT || 4000;

const app = express();
app.use(cors());
const connection = connect();

/**
 * Expose
 */

module.exports = {
  app,
  connection,
};

// Bootstrap models
fs.readdirSync(models)
  .filter((file) => ~file.indexOf('.js'))
  .forEach((file) => require(join(models, file)));

// Bootstrap routes
require('./config/express')(app);
require('./config/routes')(app);

// run cron for checking whatsapp status
schedule.scheduleJob('42 * * * *', function () {
  checkWabaStatus();
});

connection
  .on('error', console.log)
  .on('disconnected', connect)
  .once('open', listen);

function listen() {
  if (app.get('env') === 'test') return;
  app.listen(port);
}

if (app.get('env') !== 'test') {
  console.log('Express app started on port ' + port);
}

function connect() {
  var options = {
    keepAlive: 1,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  mongoose.connect(config.db, options);
  return mongoose.connection;
}
