const mongoose = require('mongoose');

const { stringConfig } = require('../../config/schema');

const SalesSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    item: stringConfig,
    value: stringConfig,
    date: stringConfig,
    category: stringConfig,
    customerEmail: stringConfig,
    orderId: stringConfig,
    itemId: stringConfig,
  },
  {
    timestamps: true,
  }
);

const Sale = mongoose.model('Sale', SalesSchema);

module.exports = { Sale };
