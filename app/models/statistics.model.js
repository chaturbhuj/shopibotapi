const mongoose = require('mongoose');

const { stringConfig, numberConfig } = require('../../config/schema');

const StatisticsSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    year: stringConfig,
    month: stringConfig,
    collectedAmount: numberConfig,
    productsSold: numberConfig,
    numOfCustomers: numberConfig,
  },
  {
    timestamps: true,
  }
);

// module.exports = mongoose.model('Merchant', MerchantSchema);
const Statistics = mongoose.model('Statistics', StatisticsSchema);

module.exports = { Statistics };
