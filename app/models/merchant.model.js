const mongoose = require('mongoose');

const { stripe } = require('../../config/stripe');

const {
  stringConfig,
  objectConfig,
  signupConfig,stringConfigOptional,
  waStatusConfig,
  botIdConfig,
} = require('../../config/schema');

const {
  PLAN_STATUSES,
  INACTIVE_STRIPE_STATUSES,
  ECOM_PLATFORMS,
  WA_STATUSES,
} = require('../utils/constants');
const { registerShopifyWebhook } = require('../utils/connectorUtils');
const { logError } = require('../../config/chalkConfig');
const { Statistics } = require('./statistics.model');
const { Customer } = require('./customer.model');
const { Sale } = require('./sales.model');
var ObjectId = require("mongodb").ObjectID;
const {
  get360DialogAccessToken,
  getAllChannels,
} = require('../utils/360utils');

const MerchantSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    name: stringConfig,
    email: stringConfig,
    company: objectConfig, //{ name , telephone , businessActivity }
    platform: stringConfig,
    company_phone_code: stringConfig,
    Customer_support_code: stringConfig,
    waNumber: stringConfig,
    sa_id: stringConfig,
    signupStatus: signupConfig,
    stripeCustomerId: stringConfig,
    credentials: objectConfig, // Different as per different platforms
    wabaStatus: waStatusConfig,
    bot_id: botIdConfig,
    City: stringConfigOptional,
    Country: stringConfigOptional,
    District: stringConfigOptional,
    Postal: stringConfigOptional,
    Region: stringConfigOptional,
    StoreAddress: stringConfigOptional,
    StoreNum: stringConfigOptional,
	
  },
  {
    timestamps: true,
  }
);

// module.exports = mongoose.model('Merchant', MerchantSchema);
const Merchant = mongoose.model('Merchant', MerchantSchema);

/**
 *
 * @param {request object} req
 * @returns {Object} merchant
 */
const createNewMerchant = async (req) => {
  try {
    const {
      merchantId,
      name,
      email,
      company,
      platform,
      waNumber,
      planActive,
      signupStatus,
      credentials,
    } = req.body;

    const customer = await stripe.customers.create({
      email,
    });

    const stripeCustomerId = customer && customer.id;

    let credentialsObject = { ...credentials };
    credentialsObject = checkStoreURL(credentials, platform);

    const merchant = new Merchant({
      merchantId,
      name,
      email,
      company,
      platform,
      waNumber,
      planActive,
      signupStatus,
      stripeCustomerId,
      credentials: credentialsObject,
      sa_id: 'no',
      wabaStatus: WA_STATUSES[0],
    });

    await merchant.save();

    let addedWebhook = true;
    if (platform === ECOM_PLATFORMS.SHOPIFY) {
      // Registering webhook for shopify private app
      addedWebhook = await registerShopifyWebhook({
        password: credentialsObject.password,
        apiKey: credentialsObject.apiKey,
        shop: credentialsObject.storeURL,
      });
    }

    if (!addedWebhook) {
      return false;
    }
    return merchant;
  } catch (error) {
    logError(error);
    return false;
  }
};

const all_merchant = async (req) => {
  
  const merchant = await Merchant.find().lean();
console.log(merchant);
  if (!merchant) {console.log('1');
    return null;
  }
console.log('2');
  return {
    ...merchant,
  };
};

const all_merchanta = async (req) => {
  
  const merchant = await Merchant.find();

  if (!merchant) {
    return null;
  }

  return merchant;
};

/**
 *
 * @param {request object} req
 * @returns {Object} Merchant object
 */
const getMerchant = async (req) => {
  const { merchantId } = req.body;
  console.log('1');
  const merchant = await Merchant.findOne({ merchantId }).lean();

  console.log('2');
  if (!merchant) {
  console.log('2.1');
    return null;
  }

  console.log('3');
  const { stripeCustomerId: customer } = merchant;

  console.log('4');
  const subscriptions = await stripe.subscriptions.list({
    customer,
  });

  console.log('5');
  const planStatus = activePlanExists(subscriptions);

  return {
    ...merchant,
    planStatus,
  };
};

const getMerchantSpd = async (req) => {
  const { merchantId } = req.body.spd;
  console.log('1');
  const merchant = await Merchant.findOne({ merchantId: ObjectId(req.body.spd) }).lean();

  console.log('2');
  if (!merchant) {
    return null;
  }


  return {
    ...merchant
  };
};

/**
 *
 * @param {Request Object} req
 * @returns null
 */
const updateMerchant = async (req) => {
  const { merchantId, updatedMerchant } = req.body;

  const merchant = await Merchant.findOneAndUpdate(
    { merchantId },
    updatedMerchant,
    {
      new: true,
    }
  );

  return merchant;
};
const update_merchaneta = async (req) => {
   const merchant = {
					
		  name: req.body.name,
		  telephone: req.body.telephone,
		  company_phone_code: req.body.company_phone_code,  
		  Customer_support_code: req.body.Customer_support_code,
		  businessActivity: req.body.businessActivity,
		  customerEmail: req.body.customerEmail,
		  customerPhone: req.body.customerPhone,
		};
      const id = req.body.Merchant;
      const merchant1 = await Merchant.updateOne(
        { _id: ObjectId(id) },
        merchant,
        {
          new: true,
        }
      );
	  
    return true;
};

/**
 *
 * @param {Request Object} req
 * @returns null
 */
const updateMerchantWithWix = async (req) => {
  const { updatedMerchant } = req.body;

  await Merchant.updateOne(
    { 'credentials.storeURL': updatedMerchant.credentials.storeURL },
    updatedMerchant
  );
};

/**
 *
 * @param {Request Object} req
 * @returns null
 */
const deleteMerchant = async (req) => {
  const { merchantId } = req.body;

  await Merchant.deleteOne({ merchantId });
};

/**
 * 
 * @param {Request Object} req
 * @returns {object} merchantObject
 */
const getMerchantContact = async (req) => {
  const { id } = req.params;
  const merchant = await Merchant.findOne({ merchantId: id })
    .select('id company')
    .lean();

  if (!merchant) {
    return null;
  }

  const {
    company: { customerEmail: customer_email, customerPhone: customer_phone },
  } = merchant;

  return {
    merchantId: id,
    customer_email,
    customer_phone,
  };
};

/**
 *
 * @param {Request Object} req
 * @returns {object} merchantObject
 */
const getMerchantStore = async (req) => {
  const { id } = req.params;
  const merchant = await Merchant.findOne({ merchantId: id })
    .select('id credentials platform')
    .lean();

  if (!merchant) {
    return null;
  }

  const {
    credentials: { storeURL: store_url },
    platform,
  } = merchant;

  return {
    merchantId: id,
    store_url,
    platform,
  };
};

/**
 *
 * @param {Request Object} req
 * @returns {object} merchantObject
 */
const getMerchantDashboard = async (req) => {
  const { merchantId, year, month } = req.body;
  const merchantStats = await Statistics.findOne({
    merchantId,
    year,
    month,
  }).lean();

  const fromDate = new Date(year, month - 1, 1);
  const toDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);

  const condition = { createdAt: { $gte: fromDate, $lte: toDate } };

  const numOfContacts = await Customer.countDocuments(condition);

  return {
    merchantId,
    collectedAmount: (merchantStats && merchantStats.collectedAmount) || 0,
    productsSold: (merchantStats && merchantStats.productsSold) || 0,
    numOfCustomers: (merchantStats && merchantStats.numOfCustomers) || 0,
    numOfContacts: numOfContacts || 0,
  };
};

/**
 *
 * @param {Request Object} req
 * @returns {number} statValue
 */
const getMerchantStat = async (req) => {
  const { merchantId, year, month, stat } = req.body;
  let statValue;

  if (stat !== 'numOfContacts') {
    const statObj = await Statistics.findOne({
      merchantId,
      year,
      month,
    })
      .select(stat)
      .lean();

    statValue = statObj && statObj[stat];

    return statValue || 0;
  } else {
    const fromDate = new Date(year, month - 1, 1);
    const toDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);

    const condition = { createdAt: { $gte: fromDate, $lte: toDate } };

    statValue = await Customer.countDocuments(condition);

    return statValue;
  }
};

/**
 *
 * @param {Request Object} req
 * @returns {sales Array} sales
 */
const getMerchantSales = async (req) => {
  const { merchantId } = req.body;

  const sales = await Sale.find({ merchantId }).sort({ createdAt: -1 }).lean();

  return sales;
};

/**
 *
 * @param {Request Object} req
 * @returns {sales Array} sales
 */
const getMerchantWaStatus = async (req) => {
  const { merchantId } = req.body;
  let status;

  const merchant = await Merchant.findOne({ merchantId })
    .select('wabaStatus waNumber')
    .lean();

  if (!merchant) {
    throw 'No such merchant exists';
  }

  // Make call to 360API if status for merchant is not verified
  if (merchant && merchant.wabaStatus !== 'verified') {
    status = await checkWAStatus(merchant);
    if (status !== merchant.wabaStatus) {
      await Merchant.updateOne({ merchantId }, { wabaStatus: status });
    }
  }

  return status;
};

const getNonBotMerchants = async () => {
  const merchants = await Merchant.find({ bot_id: { $exists: false } })
    .sort({ createdAt: -1 })
    .lean();

  return merchants;
};

/* ALL THE UTILITY FUNCTIONS ARE BELOW THIS */

/**
 * @desc Returns a boolean based on if the merchant's subscriptions has an active plan
 * @param {Object[]} subscriptions
 * @returns string
 */
const activePlanExists = (subscriptions) => {
  if (!subscriptions.data || subscriptions.data.length === 0) {
    return PLAN_STATUSES.EXPIRED;
  }
  const latestSubscription = subscriptions.data && subscriptions.data[0];
  const { status, days_until_due } = latestSubscription;

  if (INACTIVE_STRIPE_STATUSES.includes(status)) {
    return PLAN_STATUSES.EXPIRED;
  }

  return days_until_due < 2 ? PLAN_STATUSES.EXPIRING : PLAN_STATUSES.ACTIVE;
};

/**
 * @desc Returns a string based on if the merchant's wa account has been verified by 360
 * @param {Object} Merchant
 * @returns string
 */
const checkWAStatus = async (merchant) => {
  const { waNumber, wabaStatus } = merchant;
  let verStatus = wabaStatus;
  try {
    const accessToken = await get360DialogAccessToken();
    const channels = await getAllChannels(accessToken);

    //searching for the appropriate channel
    if (
      channels &&
      channels.partner_channels &&
      channels.partner_channels.length !== 0
    ) {
      for (let channel of channels.partner_channels) {
        if (
          channel &&
          channel.setup_info &&
          channel.setup_info.phone_number &&
          channel.setup_info.phone_number === waNumber
        ) {
          if (channel.status === 'created') {
            verStatus = 'in-verification';
          }
          // We might need to change the 'done' below
          else if (channel.status === 'done') {
            verStatus = 'verified';
          }
          break;
        }
      }
    }

    return verStatus;
  } catch (err) {
    logError(err);
    return verStatus;
  }
};

/**
 * @desc checks if the storeURL contains https://, if not it adds it to the URL
 * @param {Object} credentials
 * @returns {Object}
 */
const checkStoreURL = (credentials, platform) => {
  let credentialsObject = { ...credentials };
  let storeURL = credentialsObject.storeURL;

  if (platform === ECOM_PLATFORMS.WIX) {
    if (
      credentialsObject &&
      credentialsObject.storeURL &&
      !credentialsObject.storeURL.includes('https')
    ) {
      storeURL = 'https://' + credentialsObject.storeURL;
    }
  }

  if (storeURL[storeURL.length - 1] === '/') {
    storeURL = storeURL.slice(0, -1);
  }

  credentialsObject.storeURL = storeURL;
  return credentialsObject;
};
const delete_merchanta = async (req) => {
  console.log(req.body.id);
  await Merchant.deleteOne({ _id: ObjectId(req.body.id) });
  return true;
};

const get_merchenta_by_id = async (req) => {
  const { id } = req.params;
  const merchant = await Merchant.findOne({ _id: ObjectId(req.body.id) });

  if (!merchant) {
    return null;
  }

  return merchant;
};
const get_merchenta_for_product_by_id = async (req) => {
  const { merchantId } = req;
  
  const merchant = await Merchant.findOne({ merchantId: req }).lean();
	
  if (!merchant) {
    return null;
  }

  return merchant;
};
const add_sotre_address = async (req) => {
	console.log(req.body)
	const merchantId = req.body.merchantId;
	const product = {
		  City: req.body.City,
		  Country: req.body.Country,
		  District: req.body.District,
		  Postal: req.body.Postal,
		  Region: req.body.Region,
		  StoreAddress: req.body.StoreAddress,
		  StoreDept: req.body.StoreDept,
		  StoreNum: req.body.StoreNum,
	};
	
      const merchant = await Merchant.updateOne(
        { merchantId: (merchantId) },
        product,
        {
          new: true,
        }
      );
	  console.log(merchant);
    return product;

};
const testing = async (req) => {
  /* build update */
  const product = {
		  sa_id: "yes",
	};
	
      const merchant = await Merchant.updateOne(
        { _id: ObjectId('616008430e1fc737441e431e') },
        product,
        {
          new: true,
        }
      );
	  console.log(merchant);
    return product;
	
};

module.exports = {
  Merchant,
  createNewMerchant,
  all_merchant,
  all_merchanta,
  update_merchaneta,
  getMerchant,
  updateMerchant,
  deleteMerchant,
  getMerchantContact,
  updateMerchantWithWix,
  getMerchantStore,
  getMerchantDashboard,
  getMerchantStat,
  getMerchantSales,
  getMerchantWaStatus,
  getNonBotMerchants,
  checkWAStatus,
  delete_merchanta,
  getMerchantSpd,
  get_merchenta_by_id,
  get_merchenta_for_product_by_id,
  add_sotre_address,
  testing,
};
