const mongoose = require("mongoose");

const { stripe } = require("../../config/stripe");

const {
  stringConfig,
  objectConfig,
  signupConfig,
  waStatusConfig,
  botIdConfig,
} = require("../../config/schema");

const {
  PLAN_STATUSES,
  INACTIVE_STRIPE_STATUSES,
  ECOM_PLATFORMS,
  WA_STATUSES,
} = require("../utils/constants");
const { registerShopifyWebhook } = require("../utils/connectorUtils");
const { logError } = require("../../config/chalkConfig");
const { Statistics } = require("./statistics.model");
const { Product } = require("./product.model");
const { Sale } = require("./sales.model");
var ObjectId = require("mongodb").ObjectID;
const {
  get360DialogAccessToken,
  getAllChannels,
} = require("../utils/360utils");
const { Merchant } = require("./merchant.model");

const TemporderSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    productId: stringConfig,
    attributeId: stringConfig,
    statusoforder: stringConfig,
    createdtime: stringConfig,
  },
  {
    timestamps: true,
  }
);

// module.exports = mongoose.model('Merchant', MerchantSchema);
const Temporder = mongoose.model("Temporder", TemporderSchema);


const api_orderplace = async (req) => {
  try {
    
		const product = new Temporder({
		  merchantId: req.body.merchantId,
		  productId: req.body.productId,
		  attributeId: req.body.attributeId, 
		  statusoforder: 'pending', 
		  createdtime: Date.now(), 
		});
		await product.save();
		
		return product;
    
  } catch (error) {
	  console.log(error);
    logError(error);
    return false;
  }
};
const get_checkout_detail = async (req) => {
	//console.log(req.body);
	const merchant = await Temporder.findOne({ _id: ObjectId(req.body.checkout_id) });
	//console.log(merchant);
	if (!merchant) {
		return null;
	}else{
		if(merchant.statusoforder == 'pending'){
			
			console.log(merchant.productId);
			const product = await Product.findOne({ _id: ObjectId(merchant.productId) });
			return product;
		}else{
			return null;
		}
		
	}
  
	
};
const get_temporder_detail = async (req) => {
	//console.log(req.body);
	const merchant = await Temporder.findOne({ _id: ObjectId(req.body.checkout_id) });
	console.log(merchant);
	if (!merchant) {
		return null;
	}else{
		return merchant;
	}
  
	
};
const delete_temporder_detail = async (req) => {
	//console.log(req.body);
	const merchant = await Temporder.deleteOne({ _id: ObjectId(req.body.checkout_id) });
	//console.log(merchant);
	if (!merchant) {
		return null;
	}else{
		return merchant;
	}
  
	
};

module.exports = {
  Temporder,
  api_orderplace,
  get_checkout_detail,
  get_temporder_detail,
  delete_temporder_detail,
};
