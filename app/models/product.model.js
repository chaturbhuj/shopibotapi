const mongoose = require("mongoose");
const readXlsxFile = require("read-excel-file/node");
const path = require("path");

const { stripe } = require("../../config/stripe");

const {
  stringConfig,
  objectConfig,
  signupConfig,
  waStatusConfig,
  botIdConfig,
} = require("../../config/schema");

const {
  PLAN_STATUSES,
  INACTIVE_STRIPE_STATUSES,
  ECOM_PLATFORMS,
  WA_STATUSES,
} = require("../utils/constants");
const { registerShopifyWebhook } = require("../utils/connectorUtils");
const { logError } = require("../../config/chalkConfig");
const { Statistics } = require("./statistics.model");
const { Customer } = require("./customer.model");
const { Sale } = require("./sales.model");
var ObjectId = require("mongodb").ObjectID;
const {
  get360DialogAccessToken,
  getAllChannels,
} = require("../utils/360utils");
const { Merchant } = require("./merchant.model");

const ProductSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    name: stringConfig,
    shortDescription: stringConfig,
    Currency: stringConfig,
    price: stringConfig,
    Stock: stringConfig,
    image: stringConfig,
    MultipleImage: objectConfig,
    categories: stringConfig,
    Productvarient: objectConfig,
    StatusActive: stringConfig,
    ShippingType: stringConfig,
    ShippingType1: stringConfig,
    ProductCode: stringConfig,
  },
  {
    timestamps: true,
  }
);

// module.exports = mongoose.model('Merchant', MerchantSchema);
const Product = mongoose.model("Product", ProductSchema);

/**

 *
 * @param {request object} req
 * @returns {Object} merchant
 */
const createNewProduct = async (req) => {
  try {

    console.log(req.body);
    if (req.body.Productid) {
      const product = {
        name: req.body.ProductName,
        shortDescription: req.body.Description,
        price: req.body.Tax,
        Stock: req.body.Stock,
        StatusActive: req.body.StatusActive,
        ShippingType: req.body.ShippingType,
        ShippingType1: req.body.ShippingType1,
        ProductCode: req.body.ProductCode,
        Currency: req.body.Currency,
        image: req.body.Images,
        categories: req.body.Category,
        MultipleImage: req.body.MultipleImage,
        Productvarient: req.body.Productvarient,
      };
      console.log(product);
      const Productid = req.body.Productid;
      const merchant = await Product.updateOne(
        { _id: ObjectId(Productid) },
        product,
        {
          new: true,
        }
      );
      return product;
    } else {
      const product = new Product({
        name: req.body.ProductName,
        merchantId: req.body.merchantId,
        shortDescription: req.body.Description,
        price: req.body.Tax,
        Stock: req.body.Stock,
        StatusActive: req.body.StatusActive,
        ShippingType: req.body.ShippingType,
        ShippingType1: req.body.ShippingType1,
        ProductCode: req.body.ProductCode,
        Currency: req.body.Currency,
        categories: req.body.Category,
        image: req.body.Images,
        MultipleImage: req.body.MultipleImage,
        Productvarient: req.body.Productvarient,
      });
      await product.save();
      return product;
    }
  } catch (error) {
    console.log(error);
    logError(error);
    return false;
  }
};

const list_product = async (req) => {
  const merchantId = req.body.merchantId;
  const merchant = await Product.find({ merchantId });
  console.log(merchant);
  if (!merchant) {
    return null;
  }

  return merchant;
};

/* add product from excel*/ 

const upload_product = async (req, res) => {
	try {
		/* Reads from /uploads/sample.xlsx */
		const filePath = path.resolve(__dirname, "../../db/sample.xlsx");
    let merchantId = req.body.merchantId;
    return merchantId;
		let rows = await readXlsxFile(filePath, {
			dateFormat: "DD/MM/YYYY",
		});
    //console.log('rows---------->');
    //console.log(rows);
    //console.log('rows---------->');
    let product = [];
    for (let i = 1; i < rows.length; i++) {
			if (rows[i][rows[i].length - 1] != null){
				let products = await upload_excel_data(rows[i],merchantId);

        product.push(products.data);
				if(i+1 == rows.length){
					return product;
				}
			}
		}
	} catch (error) {
    console.log(error);
    logError(error);
    return false;
	}
};
/* end add product from excel*/ 
function upload_excel_data(data,merchantId) {
	return new Promise(resolve => {
    const product = new Product({
      name: data[0],
      merchantId: merchantId,
      shortDescription: data[1], 
      price: data[2],
      Stock: data[3],
      StatusActive: 'Pending',
      ShippingType: data[4],
      ShippingType1: data[5],
      ProductCode: data[6],
      Currency: data[7],
      categories: data[8],
      image: data[9],
      MultipleImage: data[10],
      Productvarient: data[11],
    });
    (async () => {
     await product.save();
    })().catch(e => console.log(e));
		resolve({ "status":200, "data":product, "message":"Added Successfully" }); 
	});
}

const delete_product = async (req) => {
  console.log(req.body.id);//Sale.find({ merchantId }).sort({ createdAt: -1 }).lean();
  await Product.deleteOne({ _id: ObjectId(req.body.id) });
  return true;
};

const get_product_by_id = async (req) => {
  const { id } = req.params;
  if (req.body.spd == 'no') {
    const merchant = await Product.findOne({ _id: ObjectId(req.body.id), merchantId: req.body.merchantId });
    if (!merchant) {
      return null;
    }
    return merchant;
  } else {
    const merchant = await Product.findOne({ _id: ObjectId(req.body.id) });
    if (!merchant) {
      return null;
    }
    return merchant;
  }



};
const get_producta_by_id = async (req) => {
  const { id } = req.params;

  const merchant = await Product.findOne({ _id: ObjectId(req.body.id) });

  if (!merchant) {
    return null;
  }

  return merchant;
};
const list_producta = async (req) => {
  const merchant_id = req.body.id;
  let filter = {};
  if (merchant_id) {
    filter["merchantId"] = merchant_id;
  }
  const merchant = await Product.aggregate([{
    $match: filter
  }, {
    $lookup: {
      from: Merchant.collection.name,
      localField: 'merchantId',
      foreignField: 'merchantId',
      as: 'murchent_details'
    }
  }]);
  console.log("merchant", req)
  console.log("merchant_id", merchant_id)
  //console.log("merchant",merchant)  

  if (!merchant) {
    return null;
  }

  return merchant;
};

const api_list_product = async (req) => {
  const page = parseInt(req.body.currentPage) || 1;
  const perPage = parseInt(req.body.pageSize) || 5;
  let currentPage = page;
  let prevPage = page === 1 ? null : page - 1;
  let pageToken = page + 1;
  let totalPages = 0;
  let postCount = 0;
  let filter = {};
  const search = req.body.search;
  if (search && search.length > 0) {
    filter["name"] = new RegExp('' + search + '', 'i');
  }
  const merchant_id = req.body.merchant_id;
  if (merchant_id && merchant_id.length > 0) {
    filter["merchantId"] = merchant_id;
  }
  const data = await Product.aggregate([{
    $match: filter
  }, {
    $lookup: {
      from: Merchant.collection.name,
      localField: 'merchantId',
      foreignField: 'merchantId',
      as: 'murchent_details'
    }
  }]).skip(perPage * page - perPage)
    .limit(perPage);
  console.log(req.body.merchant_id)

  if (!data) {
    return null;
  }

  return data;
};

const api_product_by_id = async (req) => {

  const { id } = req.params.id;
  //console.log(id);
  const merchant = await Product.findOne({ _id: ObjectId(req.params.id) });

  if (!merchant) {
    return null;
  }
  var result = [];
  merchant.Productvarient.forEach(function (item) {
    if (result.indexOf( item["VarientCode"])===-1) {
      result.push(item["VarientCode"]);
    }
  });
  var data=JSON.parse(JSON.stringify(merchant));

  console.log(result);
  let ites=[];
  for (let index = 0; index < result.length; index++) {
    const element = result[index];
    let item={key:element,values:data.Productvarient.filter((x)=>x.VarientCode==element)};
    console.log("ites",item,element)
    ites.push(item)

    
  }
  console.log("ites",ites)
  data["Productvar"]=ites;
  return data;
};

const testing = async (req) => {

  const product = {
    Currency: "CRC ",
  };

  const merchant = await Product.update(
    {},
    product,
    {
      new: true, multi: true
    }
  );
  console.log(merchant);
  return product;

};

module.exports = {
  Product,
  upload_product,
  createNewProduct,
  list_product,
  list_producta,
  delete_product,
  get_product_by_id,
  api_product_by_id,
  get_producta_by_id,
  api_list_product,
  testing,
};
