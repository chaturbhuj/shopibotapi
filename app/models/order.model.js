const mongoose = require("mongoose");

const { stripe } = require("../../config/stripe");

const {
	stringConfig,
	objectConfig,
	signupConfig,
	waStatusConfig,
	botIdConfig,
} = require("../../config/schema");

const {
	PLAN_STATUSES,
	INACTIVE_STRIPE_STATUSES,
	ECOM_PLATFORMS,
	WA_STATUSES,
} = require("../utils/constants");
const { registerShopifyWebhook } = require("../utils/connectorUtils");
const { logError } = require("../../config/chalkConfig");
const { Statistics } = require("./statistics.model");
const { Product } = require("./product.model");
const { Sale } = require("./sales.model");
var ObjectId = require("mongodb").ObjectID;
const {
	get360DialogAccessToken,
	getAllChannels,
} = require("../utils/360utils");
const { Merchant } = require("./merchant.model");

const OrderSchema = mongoose.Schema(
	{
		merchantId: stringConfig,
		productId: stringConfig,
		attributeId: stringConfig,
		amount: stringConfig,
		currency: stringConfig,
		balance_transaction: stringConfig,
		statusoforder: stringConfig,
	},
	{
		timestamps: true,
	}
);

// module.exports = mongoose.model('Merchant', MerchantSchema);
const Order = mongoose.model("Order", OrderSchema);


const succeed_order_place = async (req, attributeId, balance_transaction) => {
	//console.log(req); 
	try {

		const product = new Order({
			merchantId: req.body.merchantId,
			productId: req.body.product_id,
			attributeId: attributeId,
			amount: req.body.amount,
			currency: req.body.currency,
			balance_transaction: balance_transaction,
			statusoforder: 'success',
		});
		await product.save();
		console.log(product);
		return product;

	} catch (error) {
		console.log(error);
		logError(error);
		return false;
	}
};

const getsales = async (req) => {

	const merchantId = req.body.merchantId;
	console.log(merchantId)
	//const merchant = await Order.find({ merchantId : merchantId});
	//const merchant = await Order.findOne({ _id: ObjectId(req.body.id) });

	//console.log(merchant)
	let filter = {};
	filter["merchantId"] = merchantId;

	const merchant = await Order.aggregate([
		{
			$match: filter
		},{
			$lookup: {
				from: Merchant.collection.name,
				localField: 'merchantId',
				foreignField: 'merchantId',
				as: 'murchent_details'
			}
		}, {
			$lookup: {
				"let": { "userObjId": { "$toObjectId": "$productId" } },
				from: Product.collection.name,
				
				pipeline: [
					{ $match: {$expr: { $eq: ['$_id', '$$userObjId'] } } }
				],
				as: 'product_details'
			}
		}]);

	console.log(merchant)
	for (const m of merchant) {
		setTimeout(function () {
			console.log(m);
		}, 5000);

	}

	if (!merchant) {
		return null;
	}

	return merchant;
};

const get_collected_amount = async (req) => {
	let date = req.body.date;
	const merchantId = req.body.merchantId;
	console.log(merchantId)
	//const merchant = await Order.find({ merchantId : merchantId});
	//const merchant = await Order.findOne({ _id: ObjectId(req.body.id) });

	//console.log(merchant)
	let filter = {};
	filter["merchantId"] = merchantId;
	//filter["createdAt"] = date;

	const merchant = await Order.aggregate([
		{
			$match: filter
		},{
			$lookup: {
				from: Merchant.collection.name,
				localField: 'merchantId',
				foreignField: 'merchantId',
				as: 'murchent_details'
			}
		}, {
			$lookup: {
				"let": { "userObjId": { "$toObjectId": "$productId" } },
				from: Product.collection.name,
				
				pipeline: [
					{ $match: {$expr: { $eq: ['$_id', '$$userObjId'] } } }
				],
				as: 'product_details'
			}
		}]);

	console.log(merchant)
	for (const m of merchant) {
		setTimeout(function () {
			console.log(m);
		}, 5000);

	}

	if (!merchant) {
		return null;
	}

	collectedAmount = {
		collectedAmount:10
	}
	
	return collectedAmount;
};

const get_products_sold = async (req) => {
	let date = req.body.date;
	const merchantId = req.body.merchantId;
	console.log(merchantId)
	//const merchant = await Order.find({ merchantId : merchantId});
	//const merchant = await Order.findOne({ _id: ObjectId(req.body.id) });

	//console.log(merchant)
	let filter = {};
	filter["merchantId"] = merchantId;
	//filter["createdAt"] = date;

	const merchant = await Order.aggregate([
		{
			$match: filter
		},{
			$lookup: {
				from: Merchant.collection.name,
				localField: 'merchantId',
				foreignField: 'merchantId',
				as: 'murchent_details'
			}
		}, {
			$lookup: {
				"let": { "userObjId": { "$toObjectId": "$productId" } },
				from: Product.collection.name,
				
				pipeline: [
					{ $match: {$expr: { $eq: ['$_id', '$$userObjId'] } } }
				],
				as: 'product_details'
			}
		}]);

	console.log(merchant)
	for (const m of merchant) {
		setTimeout(function () {
			console.log(m);
		}, 5000);

	}

	if (!merchant) {
		return null;
	}

	productsSold = {
		productsSold:11
	}
	
	return productsSold;
};


const list_salesa = async (req) => {

	//console.log(merchant)
	const merchant_id = req.body.id;
	  let filter = {};
	  if (merchant_id) {
		filter["merchantId"] = merchant_id;
	  }

	const merchant = await Order.aggregate([
		{
			$match: filter
		}, {
			$lookup: {
				from: Merchant.collection.name,
				localField: 'merchantId',
				foreignField: 'merchantId',
				as: 'murchent_details'
			}
		}, {
			$lookup: {
				"let": { "userObjId": { "$toObjectId": "$productId" } },
				from: Product.collection.name,
				
				pipeline: [
					{ $match: {$expr: { $eq: ['$_id', '$$userObjId'] } } }
				],
				as: 'product_details'
			}
		}
		]);

	console.log('merchant999', JSON.stringify(merchant))


	if (!merchant) {
		return null;
	}

	return merchant;
};

const getsalesproduct = async (data) => {
	return new Promise((resolve, reject) => {
		for (let i = 0; i <= data.length; i++) {

			console.log(data[0]);
		}
		Promise.all(data).then(() => {
			resolve();
		}).catch((error) => {
			reject(error);
		})
	});

};
const merchent_count = async (req) => {
      const merchant = await Merchant.countDocuments();
      const product = await Product.countDocuments();
      const order = await Order.countDocuments();
	  console.log(merchant);
    return {merchant, product, order };
	
};
module.exports = {
	Order,
	succeed_order_place,
	getsales,
	getsalesproduct,
	get_products_sold,
	get_collected_amount,
	list_salesa,
	merchent_count,
};
