const mongoose = require('mongoose');

const { stringConfig } = require('../../config/schema');

const CustomerSchema = mongoose.Schema(
  {
    merchantId: stringConfig,
    email: stringConfig,
  },
  {
    timestamps: true,
  }
);

const Customer = mongoose.model('Customer', CustomerSchema);

module.exports = { Customer };
