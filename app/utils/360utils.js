const axios = require('axios');

/**
 * Fetch all channels from 360 dialog
 * @param {string} accessToken access token of 360 partner hub
 * @returns {Array.<Object>|object}
 */
exports.getAllChannels = (accessToken) => {
  return new Promise((resolve, reject) => {
    const config = {
      url: `https://hub.360dialog.io/v1/partners/${process.env.HUB_PARTNER_ID}/channels`,
      method: 'get',
      headers: {
        Authorization: `Bearer ${accessToken.access_token}`,
      },
    };

    axios(config)
      .then(function (response) {
        resolve(response.data);
      })
      .catch(function (error) {
        reject(error.response.data);
      });
  });
};

/**
 * Fetch access token of the 360 partner hub
 */
exports.get360DialogAccessToken = () => {
  return new Promise((resolve, reject) => {
    const data = {
      username: process.env.HUB_USERNAME,
      password: process.env.HUB_PASSWORD,
    };
    const config = {
      method: 'post',
      url: 'https://hub.360dialog.io/v1/token',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        resolve(response.data);
      })
      .catch(function (error) {
        reject(error.response.data);
      });
  });
};
