const axios = require('axios');

/**
 * Create a new bot for the merchant
 * @param {string} token Authorization token
 * @param {string} merchantId id of the merchant
 * @returns {object}
 */
exports.createBot = async (token, merchantId) => {
  const generatedBot = await this.generateBot(token);
  const { bot_id } = generatedBot;
  return new Promise((resolve, reject) => {
    const data = {
      bot_id,
      data: {
        csv_key: '1DAsizPPmN43YxN3Lxls0KX_cincKwvL7QHa6xxVFOFc',
        chat_platform: {
          D360_API_KEY: 'WD2KFw_sandbox', // TODO: To be fetched dynaically
        },
      },
      merchantId,
    };

    const config = {
      url: `${process.env.HC_SERVER_CUSTOM_URL}/api/v1/bot/add`,
      method: 'post',
      headers: {
        Authorization: token,
      },
      data,
    };

    axios(config)
      .then(function (response) {
        resolve(response.data);
      })
      .catch(function (error) {
        reject(error.response.data);
      });
  });
};

/**
 * Generate a new bot id in the system
 * @param {string} token Authorization token
 * @returns {object}
 */
exports.generateBot = (token) => {
  return new Promise((resolve, reject) => {
    const config = {
      url: `${process.env.HC_SERVER_CUSTOM_URL}/api/v1/bot/generate`,
      method: 'post',
      headers: {
        Authorization: token,
      },
    };

    axios(config)
      .then(function (response) {
        resolve(response.data);
      })
      .catch(function (error) {
        reject(error.response.data);
      });
  });
};
