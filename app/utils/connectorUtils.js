const axios = require('axios');
const { logError } = require('../../config/chalkConfig');

exports.registerShopifyWebhook = async ({ password, apiKey, shop }) => {
  try {
    if (shop && shop.includes('https')) {
      shop = shop.replace(/(^\w+:|^)\/\//, '');
    }
    const URL = `https://${apiKey}:${password}@${shop}/admin/api/2021-07/webhooks.json`;

    await axios.post(URL, {
      webhook: {
        topic: 'orders/paid',
        address: `${process.env.UNIFIED_SERVICE_URL}/webhooks/detect-order`,
        format: 'json',
      },
    });

    return true;
  } catch (error) {
    logError(error);
    return false;
  }
};

exports.registerWoocommerceWebhook = async ({
  storeURL,
  consumerKey,
  consumerSecret,
}) => {
  try {
    const URL = `${storeURL}//wp-json/wc/v3/webhooks`;

    const config = {
      url: URL,
      method: 'post',
      auth: {
        username: consumerKey,
        password: consumerSecret,
      },
      data: {
        name: 'Order created',
        topic: 'order.created',
        delivery_url: `${process.env.UNIFIED_SERVICE_URL}/webhooks/detect-order`,
      },
    };

    await axios(config);
  } catch (error) {
    logError(error);
    return false;
  }
};
