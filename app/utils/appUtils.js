const cryptoJS = require('crypto-js');
const { encryptionSalt } = require('../../config');

exports.encryptString = (string) =>
  cryptoJS.AES.encrypt(string, encryptionSalt).toString();
