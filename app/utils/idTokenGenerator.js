const axios = require('axios');

/**
 * Generate token for firebase authorization
 * @returns {string}
 */
exports.generateToken = () => {
  return new Promise((resolve, reject) => {
    const firebaseApiKey = process.env.FIREBASE_API_KEY;
    const data = JSON.stringify({
      email: process.env.FIREBASE_USERNAME,
      password: process.env.FIREBASE_PASSWORD,
      returnSecureToken: true,
    });
    const config = {
      method: 'post',
      url: `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${firebaseApiKey}`,
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        resolve(response.data.idToken);
      })
      .catch(function (error) {
        reject(error);
      });
  });
};
