const {
  Merchant,
  getNonBotMerchants,
  checkWAStatus,
} = require('../models/merchant.model');
const { createBot } = require('./bot');
const { generateToken } = require('./idTokenGenerator');
const { logError } = require('../../config/chalkConfig');

exports.checkWabaStatus = async () => {
  try {
    const token = await generateToken();
    const merchants = await getNonBotMerchants();
    let updatedMerchant = [];

    for (const merchant of merchants) {
      const { merchantId } = merchant;
      let status;
      // Make call to 360API if status for merchant is not verified
      if (merchant && merchant.wabaStatus !== 'verified') {
        status = await checkWAStatus(merchant);

        // if status becomes verified now, create a bot for that number
        if (status === 'verified') {
          updatedMerchant.push(merchantId);
          createBot(token, merchantId);
        }

        if (status !== merchant.wabaStatus) {
          await Merchant.updateOne({ merchantId }, { wabaStatus: status });
        }
      } else if (merchant && merchant.wabaStatus === 'verified') {
        // if status already verified, and bot_id is not present for that merchant
        // then, create a new bot
        updatedMerchant.push(merchantId);
        createBot(token, merchantId);
      }
    }

    console.log(
      'WABA CRON has run successfully at',
      new Date(),
      'and created bot for these merchants',
      updatedMerchant
    );
  } catch (error) {
    logError(error);
  }
};
