exports.SIGNUP_STATUSES = ['complete', 'partial'];

exports.PLAN_STATUSES = {
  ACTIVE: 'Active',
  EXPIRING: 'Expiring',
  EXPIRED: 'Expired',
};

exports.INACTIVE_STRIPE_STATUSES = [
  'incomplete',
  'incomplete_expired',
  'past_due',
  'canceled',
  'unpaid',
];

exports.ECOM_PLATFORMS = {
  WIX: 'wix',
  WOOCOMMERCE: 'woocommerce',
  SHOPIFY: 'shopify',
};

exports.STATS = [
  'productsSold',
  'collectedAmount',
  'numOfCustomers',
  'numOfContacts',
];

exports.WA_STATUSES = ['to-apply', 'in-verification', 'verified'];
