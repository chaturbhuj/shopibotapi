const admin = require('firebase-admin');
const firebaseAuth = require('../../config/firebaseServiceAccount.json');
admin.initializeApp({
  credential: admin.credential.cert(firebaseAuth),
});

/**
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @returns {Object} res
 */
exports.verifyToken = async (req, res, next) => {
  const idToken = req.headers.authorization;
  try {
    const decodedToken = await admin.auth().verifyIdToken(idToken);
    if (decodedToken) {
      req.body.merchantId = decodedToken.uid;
      return next();
    } else {
      return res.status(401).send('You are not authorized!');
    }
  } catch (e) {
    return res.status(401).send('You are not authorized!');
  }
};
