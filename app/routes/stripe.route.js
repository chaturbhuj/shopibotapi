const { validate } = require('express-validation');

const {
  createPlan,
  createSession,
  fetchDataForCustomer,
  fetchPlanForCustomer,
} = require('../validations/stripe.validation');

const { verifyToken } = require('../middlewares/auth.middleware');

module.exports = (app) => {
  const stripe = require('../controllers/stripe.controller');

  /**
   * @api {post} stripe/plans
   * @apiDescription Create a new plan for subscription
   * @apiVersion 1.0.0
   * @apiName createPlan
   * @apiGroup Stripe
   *
   * @apiParam  {String}  Plan Details
   *
   * @apiSuccess {object} Plan created details
   *
   */
  app.post('/api/stripe/plans', validate(createPlan), stripe.createPlan);

  /**
   * @api {get} stripe/plans
   * @apiDescription Fetch all plans
   * @apiVersion 1.0.0
   * @apiName fetchPlans
   * @apiGroup Stripe
   *
   * @apiSuccess {object[]} All plans list
   *
   */
  app.get('/api/stripe/plans', stripe.fetchPlans);

  /**
   * @api {post} stripe/sessions
   * @apiDescription Create a payment session
   * @apiVersion 1.0.0
   * @apiName createSession
   * @apiGroup Stripe
   *
   * @apiSuccess {object} Payment session with checkout link
   *
   */
  app.post(
    '/api/stripe/sessions',
    verifyToken,
    validate(createSession),
    stripe.createSession
  );

  /**
   * @api {get} stripe/payments
   * @get Get all payments for a customer
   * @apiVersion 1.0.0
   * @apiName fetchPayments
   * @apiGroup Stripe
   *
   * @apiSuccess {object[]} All payments for a customerId
   *
   */
  app.get(
    '/api/stripe/payments',
    verifyToken,
    validate(fetchDataForCustomer),
    stripe.fetchPayments
  );

  /**
   * @api {get} stripe/my-plan
   * @get Get latest active plan for a customer
   * @apiVersion 1.0.0
   * @apiName fetchPlan
   * @apiGroup Stripe
   *
   * @apiSuccess {object} Latest active plan of the customer
   *
   */
  app.get(
    '/api/stripe/my-plan',
    verifyToken,
    validate(fetchPlanForCustomer),
    stripe.fetchMyPlan
  );
};
