const { validate } = require('express-validation');
const {
  createMerchant,
  readMerchant,
  updateMerchant,
  deleteMerchant,
  getMerchantDashboard,
  getMerchantStatistic,
} = require('../validations/merchant.validation');
const { verifyToken } = require('../middlewares/auth.middleware');
module.exports = (app) => {
	
  const product = require('../controllers/product.controller');
  const merchant = require('../controllers/merchant.controller');
  const bulkupload = require('../controllers/bulkupload.controller');

  app.get('/product',  product.api_list_product);
  app.get('/product/:id',  product.api_product_by_id);
  app.post('/orderplace',  product.api_orderplace);
  app.post('/api/product/upload_product',  bulkupload.upload_product);

   
};
