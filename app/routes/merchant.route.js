const { validate } = require('express-validation');
const {
  createMerchant,
  readMerchant,
  updateMerchant,
  deleteMerchant,
  getMerchantDashboard,
  getMerchantStatistic,
} = require('../validations/merchant.validation');
const { verifyToken } = require('../middlewares/auth.middleware');
module.exports = (app) => {
  const merchant = require('../controllers/merchant.controller');
  const product = require('../controllers/product.controller');

  /**
   * @api {post} /api/merchant
   * @apiDescription Create a new merchant in the database
   * @apiVersion 1.0.0
   * @apiName CreateMerchant
   * @apiGroup Merchant
   *
   * @apiParam  {Object}  Merchant object
   *
   * @apiSuccess {success: boolean, merchant} Success object
   *
   */
  app.post(
    '/api/merchant',
    verifyToken,
    validate(createMerchant),
    merchant.create
  );

  /**
   * @api {get} /api/merchant
   * @apiDescription Reads a merchant from the database
   * @apiVersion 1.0.0
   * @apiName ReadMerchant
   * @apiGroup Merchant
   *
   * @apiParam  {Object}  Merchant object
   *
   * @apiSuccess {success: boolean, merchant: object} Object
   *
   */
  app.get('/api/merchant', verifyToken, validate(readMerchant), merchant.read);
  app.post('/api/getMerchantSpd', verifyToken, merchant.getMerchantSpd);
  
  app.post('/api/merchant/add_product',verifyToken, merchant.add_product);
  app.post('/api/merchant/list_product', verifyToken, merchant.list_product);
  app.post('/api/merchant/delete_product', verifyToken, merchant.delete_product);
  app.post('/api/merchant/get_product_by_id',verifyToken,  merchant.get_product_by_id);
  
  app.post('/api/merchant/list_producta', verifyToken, merchant.list_producta);
  app.post('/api/merchant/all_merchanta', verifyToken,  merchant.all_merchanta);
  app.post('/api/merchant/delete_merchanta', verifyToken,  merchant.delete_merchanta);
  app.post('/api/merchant/get_merchenta_by_id', verifyToken,  merchant.get_merchenta_by_id);
  app.post('/api/merchant/get_producta_by_id', verifyToken,  merchant.get_producta_by_id);
  app.post('/api/merchant/add_sotre_address', verifyToken,  merchant.add_sotre_address);
  app.post('/api/merchant/list_salesa', verifyToken,  product.list_salesa);
  app.post('/api/merchant/all_dashboarda', verifyToken,  merchant.all_dashboarda);
  app.post('/api/merchant/update_merchaneta', verifyToken,  merchant.update_merchaneta);
  app.post('/api/merchant/getsales',verifyToken,  product.getsales);
  app.post('/api/merchant/get_collected_amount',verifyToken,  merchant.get_collected_amount);
  app.post('/api/merchant/get_products_sold',verifyToken,  merchant.get_products_sold);
  app.post('/api/merchant/get_checkout_detail',  product.get_checkout_detail);
  app.post('/api/charge',  product.charge);
  app.get('/api/csr_data',  product.csr_data);
  //app.post('/api/merchant/testing',   merchant.testing);

  /**
   * @api {put} /api/merchant
   * @apiDescription Updates a merchant in the database
   * @apiVersion 1.0.0
   * @apiName UpdateMerchant
   * @apiGroup Merchant
   *
   * @apiParam  {Object}  Merchant object
   *
   * @apiSuccess {success: boolean} Object
   *
   */
  app.put(
    '/api/merchant',
    verifyToken,
    validate(updateMerchant),
    merchant.update
  );

  /**
   * @api {delete} /api/merchant
   * @apiDescription Deletes a merchant from the database
   * @apiVersion 1.0.0
   * @apiName DeleteMerchant
   * @apiGroup Merchant
   *
   * @apiParam  {merchantId} string
   *
   * @apiSuccess {success: boolean} Object
   *
   */
  app.delete(
    '/api/merchant',
    verifyToken,
    validate(deleteMerchant),
    merchant.delete
  );

  /**
   * @api {post} /api/merchant/credentials
   * @apiDescription Updates a woocommerce merchant in the database
   * @apiVersion 1.0.0
   * @apiName UpdateCredentials
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean} Object
   *
   */
  app.post('/api/merchant/credentials', merchant.updateCredentials);

  /**
   * @api {get} /api/merchant/:id/contact
   * @apiDescription Gets a merchant's customer contact details from the database
   * @apiVersion 1.0.0
   * @apiName getContact
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, merchantId: id, customer_email: string, customer_phone: string} Object
   *
   */
  app.get('/api/merchant/:id/contact', merchant.getContact);

  /**
   * @api {get} /api/merchant/:id/store
   * @apiDescription Gets a merchant's store details from the database
   * @apiVersion 1.0.0
   * @apiName getStore
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, merchantId: id, customer_email: string, customer_phone: string} Object
   *
   */
  app.get('/api/merchant/:id/store', merchant.getMerchantStore);

  /**
   * @api {post} /api/merchant/dashboard
   * @apiDescription Gets a merchant's statistics from the database
   * @apiVersion 1.0.0
   * @apiName getDashboard
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, merchantId: id, collectedAmount: number, productsSold: number, numOfCustomers: number , numOfContacts: number} Object
   *
   */
  app.post(
    '/api/merchant/dashboard',
    verifyToken,
    validate(getMerchantDashboard),
    merchant.getMerchantDashboard
  );

  /**
   * @api {post} /api/merchant/statistic
   * @apiDescription Gets a merchant's particular statistic from the database
   * @apiVersion 1.0.0
   * @apiName getStatistic
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, stat: number } Object
   *
   */
  app.post(
    '/api/merchant/statistic',
    verifyToken,
    validate(getMerchantStatistic),
    merchant.getMerchantStatistic
  );

  /**
   * @api {post} /api/merchant/sales
   * @apiDescription Gets a merchant's sales from the database
   * @apiVersion 1.0.0
   * @apiName getSales
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, sales: array } Object
   *
   */
  app.get('/api/merchant/sales', verifyToken, merchant.getMerchantSales);

  /**
   * @api {post} /api/merchant/wa-status
   * @apiDescription Gets a merchant's wa verification status from the database
   * @apiVersion 1.0.0
   * @apiName getWaStatus
   * @apiGroup Merchant
   *
   * @apiSuccess {success: boolean, status: string } Object
   *
   */
  app.post(
    '/api/merchant/wa-status',
    verifyToken,
    merchant.getMerchantWaStatus
  );
};
