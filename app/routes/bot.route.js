const { validate } = require('express-validation');

const { extractSearchTerm } = require('../validations/bot.validation');

module.exports = (app) => {
  const bot = require('../controllers/bot.controller');

  /**
   * @api {get} bot/extractSearchTer
   * @apiDescription Retrieve search term from user input
   * @apiVersion 1.0.0
   * @apiName ExtractSearchTerm
   * @apiGroup Bot
   *
   * @apiParam  {String}  User Input
   *
   * @apiSuccess {array[string]} Search Terms
   *
   */
  app.get(
    '/api/bot/extractSearchTerm',
    validate(extractSearchTerm),
    bot.extractSearchTerm
  );
};
