const status = require('http-status');
const csv = require('csv-parser')
const fs = require('fs')
 
const {
  createNewMerchant,
  getMerchant,
  updateMerchant,
  deleteMerchant,
  getMerchantContact,
  updateMerchantWithWix, 
  getMerchantStore,
  Merchant,
  getMerchantDashboard,
  getMerchantStat,
  getMerchantSales,
  getMerchantWaStatus,
  all_merchant,
} = require('../models/merchant.model');

const {
  createNewProduct,
  delete_product,
  list_product,
  api_product_by_id,
  get_product_by_id,
  api_list_product,
} = require('../models/product.model');
const {
  api_orderplace,
  get_checkout_detail,
} = require('../models/temporder.model');

const {
  getsales,
  getsalesproduct,
  list_salesa,
} = require('../models/order.model');

const { logError } = require('../../config/chalkConfig');
const { ECOM_PLATFORMS } = require('../utils/constants');
const { registerWoocommerceWebhook } = require('../utils/connectorUtils');
const { createPaymentIntent, stripecharge } = require('../controllers/stripe.controller')


exports.api_list_product = async (req, res) => {
  try {
    const data = await api_list_product(req);

    if (!data) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data found' });
    }

    res.status(status.OK).json({ success: true, data });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the data',
    });
  }
};
exports.getsales = async (req, res) => {
  try {
    const data = await getsales(req);
    
	console.log(data);
	
    if (!data) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data found' });
    }else{
		//const data1 = await getsalesproduct(data);
		res.status(status.OK).json({ success: true, data });
	}

    
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the data',
    });
  }
};
exports.api_product_by_id = async (req, res) => {
  try {
    const productdata = await api_product_by_id(req);
    console.log(productdata);
    if (!productdata) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data exists' });
    }

    res.status(status.OK).json({ success: true, productdata });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.api_orderplace = async (req, res) => {
  try {
    const productdata = await api_orderplace(req);
    console.log(productdata);
    if (!productdata) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data exists' });
    }
    return res.status(status.OK).json({ success: true,  checkouturl: 'http://live1.lennoxsoft.com/checkout/' + productdata._id });
  } catch (error) {
    logError(error);
    return res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.get_checkout_detail = async (req, res) => {
  try {
    console.log('get_checkout_detail');
    const productdata = await get_checkout_detail(req);
    console.log(productdata);
    if (!productdata) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data exists' });
    }else{
		
		const paymentIntent = await createPaymentIntent(req)
		return res.status(status.OK).json({ success: true, paymentIntent, productdata });
	}

    //res.status(status.OK).json({ success: true, productdata });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.createPaymentIntent = async (req, res) => {
  try {
    const paymentIntent = await createPaymentIntent(req)
    return res.status(status.OK).json({ success: true, paymentIntent });
  } catch (error) {
    logError(error);
    return res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error:  error.message || 'There was an error in fetching the merchant',
    });
  }
};

exports.charge = async (req, res) => {
  try {
    const paymentIntent = await stripecharge(req)
    return res.status(status.OK).json({ success: true, paymentIntent });
  } catch (error) {
    logError(error);
    return res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error:  error.message || 'There was an error in fetching the merchant',
    });
  }
};

exports.list_salesa = async (req, res) => {
  try {
    const data = await list_salesa(req);

    if (!data) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Data found' });
    }

    res.status(status.OK).json({ success: true, data });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the data',
    });
  }
};

exports.csr_data = async (req, res) => {
  try {
		const results = [];
		fs.createReadStream('addresses.csv')
		  .pipe(csv())
		  .on('data', (data) => results.push(data))
		  .on('end', () => {
			console.log(results);
			// [
			//   { NAME: 'Daffy Duck', AGE: '24' },
			//   { NAME: 'Bugs Bunny', AGE: '22' }
			// ]
		  });

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the data',
    });
  }
};