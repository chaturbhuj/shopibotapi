const nlp = require('compromise');
const status = require('http-status');

nlp.extend(require('compromise-sentences'));

exports.extractSearchTerm = (req, res) => {
  try {
    res
      .status(status.CREATED)
      .send(nlp(req.query.searchInput).sentences().nouns().out('array'));
  } catch (err) {
    console.log(`Error occured while extracting search term `, err);
    res
      .status(status.INTERNAL_SERVER_ERROR)
      .send(`Error occured while extracting search term `, err);
  }
};
