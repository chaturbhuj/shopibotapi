const status = require('http-status');

// const reader = require("xlsx");
// const ObjectsToCsv = require("objects-to-csv");
// const convertCsvToXlsx = require("@aternus/csv-to-xlsx");

const {
  upload_product,
} = require('../models/product.model');

const { logError } = require('../../config/chalkConfig');

exports.upload_product = async (req, res) => {
	
  try {
	 const merchant = await upload_product(req);
	 
   console.log(merchant)
    res.status(status.CREATED).json({ success: true, merchant });
  } catch (error) {console.log('e');
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in creating the merchant',
    });
  } 
};

