const status = require('http-status');

const {
  createNewMerchant,
  getMerchant,
  getMerchantSpd,
  updateMerchant,
  update_merchaneta,
  deleteMerchant,
  getMerchantContact,
  updateMerchantWithWix,
  getMerchantStore,
  Merchant,
  getMerchantDashboard,
  getMerchantStat,
  getMerchantSales,
  getMerchantWaStatus,
  all_merchant,
  delete_merchanta,
  get_merchenta_by_id,
  get_merchenta_for_product_by_id,
  all_merchanta,
  add_sotre_address,
  testing,
} = require('../models/merchant.model');

const {
  get_collected_amount,
  get_products_sold,
} = require('../models/order.model');

const {
  createNewProduct,
  delete_product,
  list_product,
  list_producta,
  get_product_by_id,
  get_producta_by_id,
} = require('../models/product.model');
const {
  merchent_count,
} = require('../models/order.model');

const { logError } = require('../../config/chalkConfig');
const { ECOM_PLATFORMS } = require('../utils/constants');
const { registerWoocommerceWebhook } = require('../utils/connectorUtils');

exports.create = async (req, res) => {
  try {
    const merchant = await createNewMerchant(req);

    if (!merchant) {
      return res.status(status.INTERNAL_SERVER_ERROR).json({
        success: false,
        error:
          'There was an error, please recheck the credentials you provided',
      });
    }

    res.status(status.CREATED).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in creating the merchant',
    });
  }
};


exports.add_product = async (req, res) => {
	
  try {
	 const merchant = await createNewProduct(req);
	 
   console.log(merchant)
    res.status(status.CREATED).json({ success: true, merchant });
  } catch (error) {console.log('e');
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in creating the merchant',
    });
  } 
};

exports.add_sotre_address = async (req, res) => {
	
  try {
	 const merchant = await add_sotre_address(req);
	 
   console.log(merchant)
    res.status(status.CREATED).json({ success: true, merchant });
  } catch (error) {console.log('e');
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in creating the merchant',
    });
  } 
};


exports.delete_product = async (req, res) => {
  try {
    const merchant = await delete_product(req);
    
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Item found' });
    }

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.list_product = async (req, res) => {
  try {
    const merchant = await list_product(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.get_product_by_id = async (req, res) => {
  try {
    const merchant = await get_product_by_id(req);
    console.log(merchant);
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.get_producta_by_id = async (req, res) => {
  try {
    const merchant = await get_producta_by_id(req);
	
    
    console.log(merchant);
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }else{
		console.log(merchant.merchantId); 
		const merchantdetail = await get_merchenta_for_product_by_id(merchant.merchantId);
		res.status(status.OK).json({ success: true, merchant , merchantdetail });
	}

    
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.get_merchenta_by_id = async (req, res) => {
  try {
    const merchant = await get_merchenta_by_id(req);
    console.log(merchant);
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.all_merchant = async (req, res) => {
	
  try {
    const merchant = await all_merchant();
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }
    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error1: error,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.read = async (req, res) => {
  try {
    const merchant = await getMerchant(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.getMerchantSpd = async (req, res) => {
  try {
    const merchant = await getMerchantSpd(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.update = async (req, res) => {
  if (
    req.body &&
    req.body.updatedMerchant &&
    (req.body.updatedMerchant.merchantId ||
      req.body.updatedMerchant.stripeCustomerId)
  ) {
    return res
      .status(status.BAD_REQUEST)
      .send('You cannot update the merchant id');
  }
  try {
    await updateMerchant(req);

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in updating the merchant',
    });
  }
};
exports.update_merchaneta = async (req, res) => {
 
  try { 
    await update_merchaneta(req);

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in updating the merchant',
    });
  }
};

exports.delete = async (req, res) => {
  try {
    await deleteMerchant(req);

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in deleting the merchant',
    });
  }
};

exports.updateCredentials = async (req, res) => {
  let mockReq = {};
  let isWix = false;
  if (
    req &&
    req.body &&
    req.body.type &&
    req.body.type === ECOM_PLATFORMS.WIX
  ) {
    const { storeURL, refreshToken, type } = req.body;
    isWix = true;

    const merchant = await Merchant.findOne({
      'credentials.storeURL': storeURL,
    })
      .select('credentials')
      .lean();
    const credentials = merchant.credentials;

    mockReq = {
      body: {
        updatedMerchant: {
          credentials: {
            storeURL,
            refreshToken,
            ...credentials,
          },
          platform: type,
        },
      },
    };
  } else {
    const {
      user_id: merchantId,
      consumer_key,
      consumer_secret,
      key_permissions,
    } = req.body;

    const merchant = await Merchant.findOne({ merchantId })
      .select('credentials')
      .lean();

    mockReq = {
      body: {
        merchantId,
        updatedMerchant: {
          credentials: {
            consumer_key,
            consumer_secret,
            key_permissions,
            storeURL:
              merchant && merchant.credentials && merchant.credentials.storeURL,
          },
        },
      },
    };
  }

  try {
    if (isWix) {
      await updateMerchantWithWix(mockReq);
    } else {
      const updatedMerchant = await updateMerchant(mockReq);
      await registerWoocommerceWebhook({
        consumerKey: updatedMerchant.credentials.consumer_key,
        consumerSecret: updatedMerchant.credentials.consumer_secret,
        storeURL: updatedMerchant.credentials.storeURL,
      });
      //Registering the woocommerce webhook
    }

    return res.status(status.OK).json({
      success: true,
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in updating the merchant',
    });
  }

  return res.status(status.OK).json({
    success: true,
  });
};

exports.getContact = async (req, res) => {
  try {
    const merchantContact = await getMerchantContact(req);

    if (!merchantContact) {
      return res
        .status(status.NOT_FOUND)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, ...merchantContact });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.getMerchantStore = async (req, res) => {
  try {
    const merchantStore = await getMerchantStore(req);

    if (!merchantStore) {
      return res
        .status(status.NOT_FOUND)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, ...merchantStore });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.getMerchantDashboard = async (req, res) => {
  try {
    const merchantStore = await getMerchantDashboard(req);

    res.status(status.OK).json({ success: true, ...merchantStore });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.getMerchantStatistic = async (req, res) => {
  try {
    const statValue = await getMerchantStat(req);

    res.status(status.OK).json({ success: true, stat: statValue });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant stats',
    });
  }
};

exports.getMerchantSales = async (req, res) => {
  try {
    const sales = await getMerchantSales(req);

    res.status(status.OK).json({ success: true, sales });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant stats',
    });
  }
};

exports.getMerchantWaStatus = async (req, res) => {
  try {
    const verStatus = await getMerchantWaStatus(req);

    res.status(status.OK).json({ success: true, status: verStatus });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant whatsapp status',
    });
  }
};

exports.list_producta = async (req, res) => {
  try {
    const merchant = await list_producta(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.all_merchanta = async (req, res) => {
	
  try {
    const merchant = await all_merchanta();
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }
    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error1: error,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.testing = async (req, res) => {
	
  try {
    const merchant = await testing();
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }
    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error1: error,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.delete_merchanta = async (req, res) => {
  try {
    const merchant = await delete_merchanta(req);
    
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Item found' });
    }

    res.status(status.OK).json({ success: true });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.all_dashboarda = async (req, res) => {
  try {
    const merchant = await merchent_count(req);
    //const product = await product_count(req);
   // const order = await order_count(req);
    
    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No Item found' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};
exports.get_collected_amount = async (req, res) => {
  try {
    const merchant = await get_collected_amount(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};

exports.get_products_sold = async (req, res) => {
  try {
    const merchant = await get_products_sold(req);

    if (!merchant) {
      return res
        .status(status.OK)
        .json({ success: 'false', data: 'No merchant exists' });
    }

    res.status(status.OK).json({ success: true, merchant });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error: 'There was an error in fetching the merchant',
    });
  }
};