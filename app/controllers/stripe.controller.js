const status = require('http-status');
const config = require('../../config');

const { stripe } = require('../../config/stripe');
const { logError } = require('../../config/chalkConfig');
const { Merchant } = require('../models/merchant.model');

const {
  createNewProduct,
} = require('../models/product.model');
const {
  api_orderplace,
  get_temporder_detail,
  delete_temporder_detail,
} = require('../models/temporder.model');
const {
  succeed_order_place,
} = require('../models/order.model');

exports.createPlan = async (req, res) => {
  try {
    const product = await stripe.products.create({
      name: req.body.productName,
    });

    await stripe.prices.create({
      unit_amount: req.body.price,
      currency: req.body.currency,
      recurring: { interval: req.body.interval },
      product: product.id,
      nickname: req.body.productName,
      metadata: { maxNoOfSales: req.body.maxNoOfSales },
    });

    res.status(status.CREATED).json({
      success: true,
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error,
    });
  }
};

exports.fetchPlans = async (req, res) => {
  try {
    const prices = await stripe.prices.list();

    res.status(status.OK).json({
      success: true,
      prices,
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error,
    });
  }
};

exports.createSession = async (req, res) => {
  try {
    const { merchantId } = req.body;

    const merchant = await Merchant.findOne({ merchantId })
      .select('stripeCustomerId')
      .lean();

    if (!merchant) {
      return res.status(status.NOT_FOUND).json({
        success: false,
        error: 'No merchant with given id exists',
      });
    }

    const { stripeCustomerId: customer } = merchant;

    const session = await stripe.checkout.sessions.create({
      success_url: config.successUrl,
      cancel_url: config.cancelUrl,
      payment_method_types: ['card'],
      customer,
      line_items: [{ price: req.body.priceId, quantity: 1 }],
      mode: 'subscription',
    });
    res.status(status.CREATED).json({
      success: true,
      session,
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error,
    });
  }
};

exports.fetchPayments = async (req, res) => {
  try {
    const { merchantId } = req.body;

    const merchant = await Merchant.findOne({ merchantId })
      .select('stripeCustomerId')
      .lean();

    if (!merchant) {
      return res.status(status.NOT_FOUND).json({
        success: false,
        error: 'No merchant with given id exists',
      });
    }

    const { stripeCustomerId: customer } = merchant;

    const payments = await stripe.paymentIntents.list({
      customer,
    });

    res.status(status.OK).json({
      success: true,
      payments,
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error,
    });
  }
};

exports.fetchMyPlan = async (req, res) => {
  try {
    const { merchantId } = req.body;
    const merchant = await Merchant.findOne({ merchantId })
      .select('stripeCustomerId')
      .lean();

    if (!merchant) {
      return res.status(status.NOT_FOUND).json({
        success: false,
        error: 'No merchant with given id exists',
      });
    }

    const { stripeCustomerId: customer } = merchant;

    const subscriptions = await stripe.subscriptions.list({
      customer,
    });

    if (!subscriptions.data || subscriptions.data.length === 0) {
      return res.status(status.OK).json({
        success: true,
        plan: null,
      });
    }

    return res.status(status.OK).json({
      success: true,
      plan: subscriptions.data[0],
    });
  } catch (error) {
    logError(error);
    res.status(status.INTERNAL_SERVER_ERROR).json({
      success: false,
      error,
    });
  }
};



exports.createPaymentIntent = async (req) => {
  const { id, amount, currency } = req.body;
  try {
    // Create a PaymentIntent with the order amount and currency
    const paymentIntent = await stripe.paymentIntents.create({
      amount: amount || 5000,
      currency:  currency || 'USD'
    });
    //store this. 
    console.log(paymentIntent)
    return {
      paymentIntent
    }

  }
  catch (err) {
    throw err;
  }
};

exports.stripecharge = async (req, res) => {
  //const { amount, currency } = req.body;
		//ProductCode: req.body."oxy"
		//checkout_id: req.body."618bc2b356fd122ec04dc0d7"
		//merchantId: req.body."lOT6Ycv2FJcYb0oMiYMRw2QhUnc2"
  //console.log(req);
  try {
    // Create a PaymentIntent with the order amount and currency
    const paymentIntent = await stripe.charges.create({
			amount: req.body.amount,
			currency: req.body.currency,
			description: req.body.description,
			source: req.body.source });
	
	if(paymentIntent.status == 'succeeded'){
		console.log('merchant1');
		const merchant1 = await get_temporder_detail(req);
		console.log('merchant2');
		//console.log(merchant1.attributeId);
		//console.log(paymentIntent.balance_transaction);
		const merchant2 = await succeed_order_place(req, merchant1.attributeId, paymentIntent.balance_transaction);
		console.log('merchant3');
		const merchant3 = await delete_temporder_detail(req);
		console.log('merchant4');
		
		
		return paymentIntent
	}else{
		return paymentIntent
	}
    

  }
  catch (err) {
    throw err;
  }
};

// const generateResponse = intent => {
//   // Generate a response based on the intent's status
//   switch (intent.status) {
//     case "requires_action":
//     case "requires_source_action":
//       // Card requires authentication
//       return {
//         requiresAction: true,
//         clientSecret: intent.client_secret
//       };
//     case "requires_payment_method":
//     case "requires_source":
//       // Card was not properly authenticated, suggest a new payment method
//       return {
//         error: "Your card was denied, please provide a new payment method"
//       };
//     case "succeeded":
//       // Payment is complete, authentication not required
//       // To cancel the payment after capture you will need to issue a Refund (https://stripe.com/docs/api/refunds)
//       console.log("💰 Payment received!");
//       return { clientSecret: intent.client_secret };
//   }
// };