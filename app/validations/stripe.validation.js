const { Joi } = require('express-validation');

module.exports = {
  // POST /api/stripe/plan
  createPlan: {
    body: Joi.object({
      productName: Joi.string().required(),
      price: Joi.number().required(),
      currency: Joi.string().required(),
      interval: Joi.string().required(),
      maxNoOfSales: Joi.number().required(),
    }),
  },

  // POST /api/stripe/sessions
  createSession: {
    body: Joi.object({
      priceId: Joi.string().required(),
      merchantId: Joi.string().required(),
    }),
  },

  // GET /api/stripe/payments
  fetchDataForCustomer: {
    body: Joi.object({
      merchantId: Joi.string().required(),
    }),
  },

  // GET /api/stripe/my-plan
  fetchPlanForCustomer: {
    body: Joi.object({
      merchantId: Joi.string().required(),
    }),
  },
};
