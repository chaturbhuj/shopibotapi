const { Joi } = require('express-validation');

module.exports = {
  // GET /api/bot/searchTerm
  extractSearchTerm: {
    query: Joi.object({
      searchInput: Joi.string().required(),
    }),
  },
};
