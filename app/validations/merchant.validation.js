const { Joi } = require('express-validation');
const { SIGNUP_STATUSES, STATS } = require('../utils/constants');

module.exports = {
  // POST /api/merchant
  createMerchant: {
    body: Joi.object({
      merchantId: Joi.string().required(),
      name: Joi.string().required(),
      email: Joi.string().required(),
      company: Joi.object().required(),
      platform: Joi.string().required(),
      waNumber: Joi.string().required(),
      credentials: Joi.object().required(),
      signupStatus: Joi.string()
        .valid(...SIGNUP_STATUSES)
        .required(),
    }),
  },

  // GET /api/merchant
  readMerchant: {
    body: Joi.object({
      merchantId: Joi.string().required(),
    }),
  },

  // PUT /api/merchant
  updateMerchant: {
    body: Joi.object({
      merchantId: Joi.string().required(),
      updatedMerchant: Joi.object().required(),
    }),
  },

  // DELETE /api/merchant
  deleteMerchant: {
    body: Joi.object({
      merchantId: Joi.string().required(),
    }),
  },

  // POST /api/merchant/dashboard
  getMerchantDashboard: {
    body: Joi.object({
      merchantId: Joi.string().required(),
      month: Joi.string().required(),
      year: Joi.string().required(),
    }),
  },

  // POST /api/merchant/dashboard
  getMerchantStatistic: {
    body: Joi.object({
      merchantId: Joi.string().required(),
      month: Joi.string().required(),
      year: Joi.string().required(),
      stat: Joi.string()
        .valid(...STATS)
        .required(),
    }),
  },
};
