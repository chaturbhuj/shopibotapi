const chalk = require('chalk');

const error = chalk.bold.red;

const logError = (string) => {
  console.log(error(string));
};

exports.logError = logError;
